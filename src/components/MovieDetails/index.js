import React, { Component, Fragment } from 'react';
import withFetchedData from '../withFetchedDataHOC';
import apiConnector from '../../servicies/ApiConnector';
import { NavLink } from 'react-router-dom';
import { isEmpty } from  'lodash';
import { Card, Icon, Alert, message } from 'antd';
import {
    MovieFooter,
    MovieContent,
    BookingForm,
    Loader,
    BrokenComponent,
} from '../';
import styles from './MovieDetails.module.scss';

class MovieDetails extends Component {
    state = {
        movie: this.props.data,
        selectedTime: null,
        selectedSetasNo: null,
        error: null,
        isSendingData: false
    }

    componentWillUnmount() {
        clearTimeout(this.state.redirectTimeout);
    }

    addBooking = async (data) => {
        this.setState({ isSendingData: true });
        try {
            await apiConnector.postBooking(data);
            this.setState({ isSendingData: false });
            message.success('Bilet został kupiony. Życzymy udanego seansu.');
            this.setState({
                redirectTimeout: setTimeout(() => this.props.history.push('/bookings'), 2000)
            });

        } catch (err) {
            this.setState({ isSendingData: false });
            message.error('Przepraszamy, ale coś poszło nie tak. Twója transacja nie została zrealizowana. Spróbój ponownie. Jeśli problem się powtórzy skontaktuje się z działem wsparcia');
        } 
    }

    handleTimeSelection = (time) => {
        this.setState({ selectedTime: time });
    }

    handleSetaSelection = (setas) => {
        this.setState({ selectedSetasNo: setas });
    }

    handleBooking = (e) => {
        e.preventDefault();
        const { selectedTime, selectedSetasNo } = this.state;
        const { params } = this.props.match;
        if (!selectedSetasNo || !selectedTime) {
            this.setState({
                error: 'Proszę wybrać ilość miejsc oraz czas seansu',
            })
            return;
        }
        this.setState({ error: null });
        const data = {
            movieId: params.id,
            reservedSetas: selectedSetasNo,
            reservedTime: selectedTime,
        }
        this.addBooking(data);
    }

    renderMovieCard() {
        const { movie, error } = this.state;
        const showTimes = movie.availableTimes.map((time, index) => ({id: index, time: time}));
        if (isEmpty(movie)) {
            return (
                <div>
                    Movie data not found :(
                </div>
            );
        }; 
        const backBtn = (
            <NavLink to="/movies">
                <Icon type="double-left" theme="outlined" /> Powrót
            </NavLink>
        ); 

        const movieCard = (
            <Card
                title={movie.title}
                extra={"Gwiazdki"}
            >
                <MovieContent {...movie} />
                <BookingForm
                    showTimes={showTimes}
                    handleBooking={this.handleBooking}
                    handleTimeSelection={this.handleTimeSelection}
                    handleSetaSelection={this.handleSetaSelection}
                    isSendingData={this.state.isSendingData}
                />
                {error &&
                    <Alert
                        className={styles.alert}
                        message="Niekompletne dane"
                        description={error}
                        type="error"
                        showIcon
                    />
                }
                <MovieFooter
                    duration={movie.duration}
                    component={backBtn}
                    classes={{
                        footerWrapper: styles.movieDetailsFooter
                    }}
                />
            </Card>
        )
        return movieCard;
    }

    render() {
        return (
            <Fragment>
                {this.renderMovieCard()}
            </Fragment>
        )
    } 
}

export default withFetchedData(MovieDetails, (api, params) => api.getMovie(params.id));
