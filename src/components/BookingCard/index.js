import React from 'react';
import { Card, Icon, Button } from 'antd';
import { MovieFooter, MovieContent } from '../';
import { formatTime, formatDate } from '../../utils';
import styles from './BookingCard.module.scss';

const BookingCard = ({handleDelete, ...booking}) => (
    <Card
        title={booking.title}
        extra={"Gwiazdki"}
        className={styles.bookingCard}
    >
        <MovieContent {...booking} />
        <div className={styles.bookingInfo}>
            <p>
                <span>Termin rezerwacji: </span><br/>
                { formatDate(booking.reservedTime) } godz. { formatTime(booking.reservedTime) }
            </p>
            <p>
                <span>Ilość miejsc: </span>
                { booking.reservedSetas }
            </p>
            <Button
                onClick={handleDelete}
                type="danger"
            >
                Usuń
            </Button>
        </div>
        <MovieFooter
            duration={booking.duration}
        />
    </Card>
);

export default BookingCard;