import React from 'react';
import { NavLink } from 'react-router-dom';
import { Card, Icon } from 'antd';
import { MovieFooter, MovieContent } from '../';
import styles from './MovieCard.module.scss';



const MovieCard = (movie) => {
    const watchBtn = (
        <NavLink to={`/movie/${movie._id}`}>
            Oglądam <Icon type="double-right" theme="outlined" />
        </NavLink>
    )
    return (
        <Card
            title={movie.title}
            extra={"Gwiazdki"}
            className={styles.movieCard}
        >
            <MovieContent {...movie} />
            <MovieFooter
                duration={movie.duration}
                component={watchBtn}
            />
        </Card>
    );
}

export default MovieCard;