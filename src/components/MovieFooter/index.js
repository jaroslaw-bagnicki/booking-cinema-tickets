import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'antd';
import styles from './MovieFooter.module.scss';

const MovieFooter = ({ duration, component, classes }) => (
    <div className={`${styles.movieFooter} ${classes.footerWrapper}`}>
        <div>
            <Icon type="clock-circle" theme="outlined" /> { duration } min
        </div>
        { component && component }
    </div>
);

MovieFooter.defaultProps = {
    classes: {
        footerWrapper: "",
    }
}

export default MovieFooter;
