import React from 'react';
import { Form, Button, Select, InputNumber } from 'antd';
import { formatTime } from '../../utils';
import styles from './BookingForm.module.scss';

const BookingForm = ({
    showTimes,
    handleBooking,
    handleTimeSelection,
    handleSetaSelection,
    isSendingData
}) => {
    const FormItem = Form.Item;
    const Option = Select.Option;
    const timeOptions = showTimes.map(showTime =>
    <Option key={showTime.id} value={showTime.time}>{formatTime(showTime.time)}</Option>);
    return (
        <Form className={styles.bookingForm} onSubmit={handleBooking}>
            <FormItem className={styles.fullWidth} label="Dostępne godziny:">
                <Select
                    showSearch
                    placeholder="Wybierz czas"
                    onChange={handleTimeSelection}
                >
                    {timeOptions}
                </Select>
            </FormItem>
            <FormItem className={styles.fullWidth} label="Ilość miejsc:">
                <InputNumber
                    className={styles.fullWidth}
                    defaultValue={0} 
                    onChange={handleSetaSelection}
                />
            </FormItem>
            <FormItem className={styles.hideLabel} label="Submit">
                <Button
                    type="primary"
                    htmlType="submit"
                    disabled={isSendingData}
                >
                    Kup bilet
                </Button>
            </FormItem>
        </Form>
    )
};

const wrappedBookingForm = Form.create()(BookingForm);

export default wrappedBookingForm;