import React, { Component } from 'react';
import { Row, Col } from 'antd';
import withFetchedData from '../withFetchedDataHOC';
import { isEmpty } from  'lodash';
import { MovieCard, BrokenComponent } from '../';
import styles from './MovieList.scss';

class MovieList extends Component {
    state = {
        movies: this.props.data,
    }

    renderMovies() {
        const { movies } = this.state;
        if (isEmpty(movies)) {
            return null;
        }; 
        const moviesList = movies.map(movie => (
            <Col key={movie['_id']} className="gutter-row" span={12}>
                <MovieCard {...movie} />
            </Col>
        ));
        return moviesList;
    }

    render() {
        return (
            <Row gutter={16}>
                {this.renderMovies()}
            </Row>
        )
    }
}

export default withFetchedData(MovieList, (api) => api.getMovies());