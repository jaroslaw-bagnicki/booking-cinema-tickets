import React, { Component } from 'react';
import apiConnector from '../../servicies/ApiConnector';
import Loader from '../Loader';
import { message } from 'antd';

const withFetchedData = (WrappedComponent, selectResourceFn) => {

   class WithFeededData extends Component {

        constructor(props) {
            super(props);
            this.state = {
                isLoadingData: true,
                data: null,
            };
            this.selectResourceFn = selectResourceFn;
        }
    
        fetchData = async (selectResourceFn) => {
            const { params } = this.props.match;
            this.setState({ isLoadingData: true });
            try {
                const { data } = await selectResourceFn(apiConnector, params);
                this.setState({
                    isLoadingData: false,
                    data: data
                })
            } catch (err) {
                this.setState({ isLoadingData: false });
                message.error('Przepraszamy ale nie udało się pobrać zasobów. Spróbuj później ...');
            }
        }
  
        componentDidMount() {
            this.fetchData(this.selectResourceFn);
        }
    
        render() {
            return this.state.isLoadingData === true ?
                <Loader />
            :
            <WrappedComponent data={this.state.data} {...this.props}  />
        }
    }

    WithFeededData.displayName = `WithFeededData(${getDisplayName(WrappedComponent)})`;

    return WithFeededData;
}

function getDisplayName(WrappedComponent) {
    return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}

export default withFetchedData;

