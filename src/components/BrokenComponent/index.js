import React from 'react';
import { Alert } from 'antd';
import styles from './BrokenComponent.module.scss';

const BrokenComponent = () => (
    <Alert
      message="Wystąpił błąd"
      description="Nasz zespół pracuje nad usunięciem błedu."
      type="warning"
      showIcon
    />
);

export default BrokenComponent;
