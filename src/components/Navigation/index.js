import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Menu, Icon } from 'antd';

class Navigation extends Component {
    state = {
        current: 'movies',
    }
    handleClick = (e) => {
        if (e.key === 'soon') {
            this.setState({
                current: 'movies',
            });
            return;
        }
        this.setState({
            current: e.key,
        });
    }
    render() {
        return (
            <Menu
                onClick={this.handleClick}
                selectedKeys={[this.state.current]}
                mode="horizontal"
            >
                <Menu.Item key="movies">
                    <NavLink to="/movies">
                        <Icon type="video-camera" /> Movie List
                    </NavLink>
                </Menu.Item>
                <Menu.Item key="bookings">
                    <NavLink to="/bookings">
                        <Icon type="book" /> My Bookings
                    </NavLink>
                </Menu.Item>
                <Menu.Item key="soon">
                    <NavLink to="/redirect">
                        <Icon type="lock" /> Cooming Soon
                    </NavLink>
                </Menu.Item>
            </Menu>
        )
    }
}

export default Navigation;