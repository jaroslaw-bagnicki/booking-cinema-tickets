import React, { Component } from 'react';
import { Row, Col, message } from 'antd';
import withFetchedData from '../withFetchedDataHOC';
import apiConnector from '../../servicies/ApiConnector';
import { BookingCard, BrokenComponent } from '../';
import styles from './BookingList.module.scss';

class BookingList extends Component {
    state = {
        bookings: this.props.data,
    }

    handleDelete = async (id) => {
        try {
            await apiConnector.deleteBooking(id);
            message.success('Rezerwacja została usunięta');
            this.props.history.go(0);
        } catch (err) {
            message.error('Przepraszamy, ale coś poszło nie tak. Spróbuj jeszcze raz.');
        } 
    }

    renderBookings() {
        const bookings = this.state.bookings.map(booking => (
            <Col key={booking.bookingId} className="gutter-row" span={12}>
                <BookingCard
                    {...booking}
                    handleDelete={() => this.handleDelete(booking.bookingId)}
                />
            </Col>
        ));
        return bookings;
    }

    render() {
        return (
            <Row gutter={16}>
                 {this.renderBookings()}
            </Row>
        )
    }
}

export default withFetchedData(BookingList, (api) => api.getBoookings());