import React from 'react';
import { formatDate } from '../../utils';
import styles from './MovieContent.module.scss';

const MovieContent = (movie) => (
    <div className={styles.movieContent}>
        <img src={movie.image} alt={movie.title} />
        <div>
            <p>
                <span>Data premiery: </span>
                { formatDate(movie.releaseDate) }
            </p>
            <p>
                <span>Opis filmu: </span>
                { movie.description }
            </p>
        </div>
    </div>
);

export default MovieContent;
