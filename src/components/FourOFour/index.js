import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from './FourOFour.scss';

const FourOFour = () => (
    <div>
        <h1>404 - Page not found.</h1>
        <p>Go back to <NavLink to="/">home page</NavLink></p>
    </div>
);

export default FourOFour;