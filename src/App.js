import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Layout } from 'antd';
import routesConfig from './routes';
import { Navigation, FourOFour } from './components';
import styles from './App.module.scss';

const App = () => {
  const { Content } = Layout;
  const routes = routesConfig.routes.map(({
    id,
    path,
    exact,
    component,
  }) => (
    <Route
      key={id}
      path={path}
      component={component}
      exact={exact}
    />
  ));
  const redirects = routesConfig.redirects.map(({
    id,
    from,
    to,
    exact,
  }) => (
    <Redirect
      key={id}
      from={from}
      to={to}
      exact={exact}
    />
  ))
  return (
    <div className={styles.fullHeight}>
      <Navigation />
      <Layout className={styles.layout}>
        <Content className={styles.content}>
          <Switch>
            { routes }
            { redirects }
            <Route path="*" component={FourOFour} />
          </Switch>
        </Content>
      </Layout>
    </div>
  )
}

export default App;
