import axios from 'axios';
import { api } from '../../config';
import ApiConnector from './ApiConnectorClass.js'

const apiConnector = new ApiConnector(
    axios.create({ baseURL: api.baseUrl }), 
    api.userId, 
    Date.now()
  );

  console.log('Api connector module loaded.');
  apiConnector.logCreateToken();

export default apiConnector;
