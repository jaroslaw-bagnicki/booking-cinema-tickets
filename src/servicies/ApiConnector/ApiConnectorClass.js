class ApiConnector {
  constructor(axiosInstance, userId, apiToken) {
    this.api = axiosInstance;
    this.userId = userId;
    this.apiToken = apiToken;
  }

  logCreateToken() {
    console.log('Api token: ', this.apiToken);
  }

  getMovies() {
    this.logCreateToken();
    return this.api.get('movies');
  }

  getMovie(id) {
    this.logCreateToken();
    return this.api.get(`movies/${id}`);
  }

  getBoookings() {
    this.logCreateToken();
    return this.api.get(`mybookings/${this.userId}`)
  }

  postBooking(data) {
    this.logCreateToken();
    return this.api.post('bookings', {...data, userId: this.userId});
  }

  deleteBooking(id) {
    this.logCreateToken();
    return this.api.delete(`bookings/${id}`);
  }
}

export default ApiConnector;