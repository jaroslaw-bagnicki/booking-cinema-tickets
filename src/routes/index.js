import {
    MovieList,
    MovieDetails,
    BookingList,
} from '../components';

export default {
    routes: [
        {
            id: 1,
            path: '/movies',
            component: MovieList,
            exact: true,
        },
        {
            id: 2,
            path: '/movie/:id',
            component: MovieDetails,
            exact: true,
        },
        {
            id: 3,
            path: '/bookings',
            component: BookingList,
            exact: true,
        },
    ],
    redirects: [
        {
            id: 1,
            from: '/',
            to: '/movies',
        },
        {
            id: 2,
            from: '/redirect',
            to: '/movies',
        },
    ]
}